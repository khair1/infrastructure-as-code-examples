resource "aws_security_group" "HTTP_HTTPS_SSH" {
#  name = "${var.SG-Prefix}http_https_ssh-${timestamp()}"
  name = "${var.SG-Prefix}http_https_ssh-${random_string.zone.result}"

  # tags {
  #       #Name = "${var.SG-Prefix}http_https_ssh-${timestamp()}"
  #       Name = "${var.SG-Prefix}http_https_ssh-${random_string.zone.result}"
  # }
  description = "HTTP, HTTPS & SSH CONNECTIONS INBOUND (managed by Terraform)"

  vpc_id = "${data.aws_vpc.default.id}"

  ingress {
         from_port = 443
         to_port = 443
         protocol = "TCP"
         cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
       from_port = 80
       to_port = 80
       protocol = "TCP"
       cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "Allow_ICMP_GL" {
  name = "${var.SG-Prefix}allow_ICMP_GL-${random_string.zone.result}"
  # tags {
  #     Name =  "${var.SG-Prefix}allow_ICMP_GL-${random_string.zone.result}"
  #     }

description = "Allow ICMP connections (managed by Terraform)"
vpc_id = "${data.aws_vpc.default.id}"


ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
#  cidr_blocks = ["${aws_instance.OpenLDAP.private_ip}/32"]
  }
egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}
